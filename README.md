# AWS EPAM homework lab via terraform

** WARNING: Do not use in PROD!!! labs only for example with unsecure solutions **

Requirements:

before **terraform apply** create and place aws credentials + ssh priv and pub key files in .aws/ dir:

```
.aws/
├── credentials
├── id_rsa_private_key
└── id_rsa.pub
```

Don't forget **terraform init** first...

After terraform apply - use "aws_elb.web_lb.dns_name" from Output and type it in browser

_Default login: wpadmin_
_Default pass: 123qwe_

For debug use public_ip from output:

_ssh -i .aws/id_rsa_private_key -l ubuntu aws_instance.web_server_a.public_ip_
