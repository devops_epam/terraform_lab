/*
Only define variables
*/

variable "region" {
    description = "Please Enter AWS Region to deploy Server"
    type = string
    default = "us-east-1"
}

variable "linux_ami_id" {
    description = "Default Ubuntu from free-tier"
    type = string
    default = "ami-09e67e426f25ce0d7"
}

variable "instance_type" {
    description = "Enter Instance Type"
    type = string
    default = "t2.micro"
}

variable "common_tags" {
  description = "Common Tags to apply to all resources"
  type = map
    default = {
    Project     = "Epam AWS homework"
    Environment = "testing"
    }
}

variable "pass_store" {
    type = string
    default = "123qwe"
    sensitive = true
}
