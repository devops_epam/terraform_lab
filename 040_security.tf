/*
Security initializing...
*/

resource "aws_key_pair" "ssh_key" {
  key_name   = "ssh_key"
  public_key = file(".aws/id_rsa.pub")
}

// Generate Password
resource "random_string" "rds_password" {
  length           = 12
  special          = false

  keepers = {
    kepeer1 = var.pass_store
  }
}

// Store Password in SSM Parameter Store
resource "aws_ssm_parameter" "rds_password" {
  name        = "/prod/mysql"
  description = "Master Password for RDS MySQL"
  type        = "SecureString"
  value       = random_string.rds_password.result
}

resource "aws_security_group" "allow_web_sg" {
  name = "allow_www"
  description = "Security group WWW/SSH access"
  vpc_id = "${aws_vpc.main_network.id}"
  tags = var.common_tags
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "SSH access"
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_sql_sg" {
  name = "allow_sql"
  description = "Security group SQL access"
  vpc_id = "${aws_vpc.main_network.id}"
  tags = var.common_tags
  ingress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = [aws_subnet.subnet_a.cidr_block, aws_subnet.subnet_b.cidr_block]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "load_balancer_sg" {
  name   = "LoadBalancerSG"
  description = "Security group of LoadBalancer"
  vpc_id = aws_vpc.main_network.id
  tags = var.common_tags
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "HTTP access"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "nfs_sg" {
  name   = "allow_nfs"
  description = "Security group NFS access"
  vpc_id = aws_vpc.main_network.id
  tags = var.common_tags
  ingress {
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.subnet_a.cidr_block, aws_subnet.subnet_b.cidr_block]
    description = "NFS access"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}