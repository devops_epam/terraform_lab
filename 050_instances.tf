/*
Creating instances
*/

resource "aws_instance" "web_server_a" {
  ami = var.linux_ami_id
  instance_type = var.instance_type
  key_name = aws_key_pair.ssh_key.id
  vpc_security_group_ids = [aws_security_group.allow_web_sg.id]
  subnet_id = aws_subnet.subnet_a.id
  user_data = <<-EOF
            #!/bin/bash
            apt update && apt -y upgrade
            apt -y install apache2 nfs-common php php-mysql php-zip php-intl php-xmlrpc php-soap php-xmlrpc php-xml php-mbstring php-gd php-curl mysql-client
            mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${aws_efs_mount_target.nfs_subnet_a.dns_name}:/ /var/www/html/
            curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
            chmod +x wp-cli.phar && mv wp-cli.phar /usr/local/bin/wp-cli
            chown www-data:www-data /var/www/html && chown www-data:www-data /var/www/
            sudo systemctl enable --now apache2
          EOF 
  depends_on = [aws_db_instance.mysql_server, aws_efs_mount_target.nfs_subnet_a]
  tags = merge(var.common_tags, { Name = "Web server A" })
}

resource "aws_instance" "web_server_b" {
  ami = var.linux_ami_id
  instance_type = var.instance_type
  key_name = aws_key_pair.ssh_key.id
  vpc_security_group_ids = [aws_security_group.allow_web_sg.id]
  subnet_id = aws_subnet.subnet_b.id
  user_data = <<-EOF
            #!/bin/bash
            apt update && apt -y upgrade
            apt -y install apache2 nfs-common php php-mysql php-zip php-intl php-xmlrpc php-soap php-xmlrpc php-xml php-mbstring php-gd php-curl mysql-client
            mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport ${aws_efs_mount_target.nfs_subnet_a.dns_name}:/ /var/www/html/
            echo ${aws_efs_mount_target.nfs_subnet_b.dns_name}:/ /var/www/html/ nfs defaults,_netdev 0 0 >> /etc/fstab
            curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
            chmod +x wp-cli.phar && sudo mv wp-cli.phar /usr/local/bin/wp-cli
            chown www-data:www-data /var/www/html && chown www-data:www-data /var/www/
            sudo systemctl enable --now apache2
          EOF 
  depends_on = [aws_db_instance.mysql_server, aws_efs_mount_target.nfs_subnet_b]
  tags = merge(var.common_tags, { Name = "Web server B" })
}

resource "aws_db_instance" "mysql_server" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  name                   = "wordpress"
  username               = "wpadmin"
  password               = data.aws_ssm_parameter.my_rds_password.value
  parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = "true"
  db_subnet_group_name   = aws_db_subnet_group.www_subnets_group.name
  availability_zone = data.aws_availability_zones.working.names[0]
  vpc_security_group_ids = [aws_security_group.allow_sql_sg.id]
  depends_on = [aws_security_group.allow_sql_sg]
  tags = merge(var.common_tags, { Name = "SQL server" })
}