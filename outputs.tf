/*
Only outputs from AWS
*/

output "web_server_a" {
    value = aws_instance.web_server_a.public_ip
    description = "IP of web server A"
}

output "web_server_b" {
    value = aws_instance.web_server_b.public_ip
    description = "IP of web server B"
}

output "nfs_dns" {
    value = aws_efs_mount_target.nfs_subnet_a.dns_name
    description = "NFS DNS"
}

output "mysql_address" {
    value = aws_db_instance.mysql_server.address
    description = "MySQL address"
}

output "elb_dns_name" {
    value = aws_elb.web_lb.dns_name
    description = "The DNS name of the ELB"
}
