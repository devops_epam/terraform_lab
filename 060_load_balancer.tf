/*
Load balance ELB
*/

resource "aws_elb" "web_lb" {
    name = "LoadBalancer"
    #availability_zones = [data.aws_availability_zones.working.names[0], data.aws_availability_zones.working.names[1]]
    internal           = false
    security_groups    = [aws_security_group.load_balancer_sg.id]
    subnets            = [aws_subnet.subnet_a.id, aws_subnet.subnet_b.id]
    instances          = [aws_instance.web_server_a.id, aws_instance.web_server_b.id]
    depends_on         = [aws_instance.web_server_a, aws_instance.web_server_b]
    listener {
        lb_port = 80
        lb_protocol = "http"
        instance_port = 80
        instance_protocol = "http"
    }
    health_check {
        healthy_threshold   = 2
        unhealthy_threshold = 2
        timeout             = 3
        target              = "HTTP:80/"
        interval            = 30
    }
    tags = merge(var.common_tags, { Name = "Load balancer based on ELB" })
}

resource "time_sleep" "wait_3_minutes" {
  depends_on = [aws_instance.web_server_a, aws_elb.web_lb]
  create_duration = "180s"
}

resource "null_resource" "ec2-ssh-connection_4_install_wp" {
    depends_on = [aws_instance.web_server_a, aws_elb.web_lb, time_sleep.wait_3_minutes]
    triggers = {
        always_run = "${timestamp()}"
    }
    provisioner "remote-exec" {
        inline = [
        "cd /var/www/html && sudo -u www-data /usr/local/bin/wp-cli core download",
        "cd /var/www/html && sudo -u www-data /usr/local/bin/wp-cli config create --dbhost=${aws_db_instance.mysql_server.address} --dbname=wordpress --dbuser=wpadmin --dbpass=${data.aws_ssm_parameter.my_rds_password.value} --locale=en_DB",
        "cd /var/www/html && sudo -u www-data /usr/local/bin/wp-cli core install --url=${aws_elb.web_lb.dns_name} --title=EPAM-AWS-Lab --admin_user=wpadmin --admin_password=${var.pass_store} --admin_email=example@epam.com",
        "sudo systemctl restart apache2"
        ]
        connection {
        host        = aws_instance.web_server_a.public_ip
        type        = "ssh"
        port        = 22
        user        = "ubuntu"
        private_key = "${file(".aws/id_rsa_private_key")}"
        timeout     = "1m"
        agent       = false
        }
    }
}
