/*
Netwroks init
*/
resource "aws_vpc" "main_network" {
  cidr_block       = "10.0.0.0/16"
  assign_generated_ipv6_cidr_block = false
  instance_tenancy = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  tags = merge(var.common_tags, { Name = "Main Network" })
}

resource "aws_subnet" "subnet_a" {
  vpc_id = aws_vpc.main_network.id
  cidr_block       = "10.0.10.0/24"
  # turn it on for debug via public IP:
  map_public_ip_on_launch = true
  availability_zone = data.aws_availability_zones.working.names[0]
  tags = merge(var.common_tags, { Name = "Subnet A" })
}

resource "aws_subnet" "subnet_b" {
  vpc_id = aws_vpc.main_network.id
  cidr_block       = "10.0.20.0/24"
  # turn it on for debug via public IP:
  map_public_ip_on_launch = true
  availability_zone = data.aws_availability_zones.working.names[1]
  tags = merge(var.common_tags, { Name = "Subnet B" })
}

resource "aws_db_subnet_group" "www_subnets_group" {
  name       = "db_subnet_group"
  subnet_ids = [aws_subnet.subnet_a.id, aws_subnet.subnet_b.id]
  tags = merge(var.common_tags, { Name = "Group of subnets: A and B" })
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.main_network.id
  tags = merge(var.common_tags, { Name = "Internet Gateway" })
}

resource "aws_default_route_table" "default_routing" {
  default_route_table_id = aws_vpc.main_network.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }
  tags = merge(var.common_tags, { Name = "Default routing table" })
}

resource "aws_efs_file_system" "nfs_wordpress" {
  creation_token = "wordpress_efs"
  tags = merge(var.common_tags, { Name = "NFS for wordpress" })
}

resource "aws_efs_mount_target" "nfs_subnet_a" {
  file_system_id  = aws_efs_file_system.nfs_wordpress.id
  subnet_id       = aws_subnet.subnet_a.id
  ip_address      = "10.0.10.100"
  security_groups = [aws_security_group.nfs_sg.id]
  depends_on = [aws_efs_file_system.nfs_wordpress]
}

resource "aws_efs_mount_target" "nfs_subnet_b" {
  file_system_id  = aws_efs_file_system.nfs_wordpress.id
  subnet_id       = aws_subnet.subnet_b.id
  ip_address      = "10.0.20.100"
  security_groups = [aws_security_group.nfs_sg.id]
  depends_on = [aws_efs_file_system.nfs_wordpress]
}